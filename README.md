Prudential Datarobot installation
----------------------------------

Ansible playbook to Install prerequisties, download the binary of Datarobot, untar and install it.

Variables that can be passed dynamically during playbook execution
-------------------------------------------------------------------

__run_as                        - User to run the playbook                                                    
__datarobot_user_name           - Datarobot app username                                                      
__datarobot_grp_name            - Datarobot app group                                                         
__docker_grp_name               - Docker group name                                                           
__csr_privatekey_password       - Private key Passphrase                                                      
__csr_common_name               - CSR Common name                                                             
__app_server_ip                 - Application server IP                                                       
__prediction_server_ip          - Prediction server IP                                                        
__ssl_cert_dir                  - Directory where Private Key and CSR is created                              

Running Playbook Example
------------------------

ansible-playbook -i inventories -e "__host_group_all=App-UAT,Prediction-UAT __host_group_app=App-UAT __csr_common_name=datarobot-pca" datarobot_playbook.yml 

group_vars
------------------

Update the server IPs in respective group_vars file. Example:

__app_server_ip: 10.0.0.4
__prediction_server_ip: 10.0.0.5
__app_server_host: ahklifeludar002
__prediction_server_host: ahklifeludar003

Inventory
----------

Update hostnames in inventories/hosts file. Example:

[App-UAT]
ahklifelddar004

[Prediction-UAT]
ahklifelddar005

Roles
-------
Below are the roles used in this playbook

* datatobot_prereqisites -- Installs the pre-requisites required for deploying Datarobot.
* datarobot_install -- Download and Untars the binary of Datarobot and install datarobot.
* datarobot_uninstall -- uninstall datarobot.
* dadarobot_ssl -- Configure SSL certificate

Prudential Datarobot uninstallation
------------------------------------

Run the following playbook to uninstall datarobot

ansible-playbook -i inventories -e "__host_group_all=App-UAT,Prediction-UAT" datarobot_uninstall.yml

Prudential Datarobot SSL Configuration
--------------------------------------

Run the following playbook to configure the ssl certificate

ansible-playbook -i inventories -e "__host_group_all=App-UAT,Prediction-UAT" datarobot_ssl.yml

